# howtos

Documentation for how to do things

So this is a collection of things that I have done where there are a lot of moving parts involved. I also seem to find a lot of examples of things which are *almost* but not quite what I want, or where the process has changed slightly, or where a new version of something has changed the procedure, or what have you.

Things I would eventually like to have here:

- Spin up an EC2 instance with some kind of DB and python environment
- Get a silly script online quickly and cheaply, but somewhere that could become permanent if you want
- Spell out config details for writing MX records and similar
- DB schema for things I might want to do again

Early versions are likely to be messy, full of typos, and unfinished just so you know. They might not even work or I could be doing things in a completely unintuitive or redundant or needlessly complicated way. Those are the breaks.

If you have suggestions or comments please let me know! I'm easiest to find on Mastodon https://mastodon.social/@pagrus