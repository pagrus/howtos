# Flask - Nginx - EC2 - git - Postgres (?)

So maybe I'm misuderstanding something but a lot of the "getting started with flask" type tutorials have you develop something locally and then push it up to a server. I would rather have a minimal app up and working that I can then fool around with until it does what I want. Maybe I'm just not looking hard enough, but well that's how it goes.

Anyway. This aims to be a guide to getting a Flask hello world up and online where you can fiddle with it until you get tired of it or it turns into something you want to keep around. It's worth mentioning that for a while I wanted to avoid Flask altogether and just write Python GCI, but I'm starting to warm up to Flask. 

I doubt I'll get to the Postgres part any time soon so if you have an assignment that needs to be done this weekend you might have better luck elsewhere.

Nginx seems like a good thing to start with, I may change my mind at some point and swithch to Apache. Who knows!

## prerequisites/assumptions

There are a *lot* of guides that cover the minimum things you need, so if you struggle with the following items you will want to do a little review. This stuff will not be covered here

- have an aws account
- be able to start up a plain ubuntu ec2 instance. specs can be minimal
- have aws credentials or be able to generate some as you need them
- have somewhere you can stick a git repo and ssh credentials on hand. I don't think it will matter if it's GitHub vs GitLab vs Bitbucket or whatever
- ???

## start up an instance

I just use whatever ubuntu ami is the default or at the top of the list amazon gives you. you can use defaults for everything, change what you want. This is what those defaults wound up being:

- Ubuntu Server 18.04 LTS (HVM), SSD Volume Type - ami-063aa838bd7631e0b
- t2.micro
- Step 3: Configure Instance Details: blah blah blah
- Step 4: Add Storage: ok I changed this to 100GB. I don't think it makes much of a difference though
- Step 5: Add Tags: nope. you can if you want though
- Step 6: Configure Security Group: pretty sloppy here, but you can always make it nicer 
    - SSH - TCP - 22 - 0.0.0.0/0
    - HTTP - TCP - 80 - 0.0.0.0/0, ::/0
- Step 7: Review Instance Launch: ok! looks good...
- select or generate a key pair
- start it up, you can name it something if you want

## connect over ssh, install stuff

Ok I am assuming you can find your machine ip address and know how to SSH, if not I'll put something here

- `sudo apt-get update`
- `sudo apt-get upgrade` 

### install nginx

https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-18-04

`sudo apt install nginx`

test by visiting ip

dns stuff if you want, i'm not going to bother right now

### install pip 'n' stuff

https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-uswgi-and-nginx-on-ubuntu-18-04

`sudo apt install python3-pip python3-dev build-essential libssl-dev libffi-dev python3-setuptools`

## virtual env stuff

- `sudo apt install python3-venv`
- mkdir ~/myproject
- cd ~/myproject
- python3.6 -m venv myprojectenv
- source myprojectenv/bin/activate

## start flasking

- pip install wheel
- pip install uwsgi flask 

## create a test app

pico ~/myproject/myproject.py

```from flask import Flask
app = Flask(__name__)

@app.route("/")
def hello():
    return "<p>flerp! urggggg</p>"
    
if __name__ == '__main__':
   app.run('0.0.0.0',80, debug = True)```

then try
   
`python myproject.py`

hmmm permission error, hmmmmmm even with port 5000, although browser just hangs in that case


